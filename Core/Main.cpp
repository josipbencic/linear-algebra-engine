#include "../Core/Platform.hpp"
#include "../Core/StateMachine.hpp"
#include "../Core/InputManager.hpp"

#include "../Engine/AlgebraicStructures/RealCoordinateSpace.hpp"
#include "../Engine/AlgebraicStructures/Mat3.hpp"
#include "../Engine/AlgebraicStructures/Polynomial.hpp"

#include "../Engine/Algorithms/GramSchmidt.hpp"

#include "../Engine/MathStreams.hpp"

#include <iostream>
#include <vector>
#include <fstream>

using namespace std;
using namespace math;

/*
int main() {

	StateMachine app;
	while (1) app.showScene();
	return 0;
}
*/
