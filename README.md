# README #

### ABOUT ###

This is a Linear Algebra exam solver.
In 2016, I was completing Linear Algebra 2 course at University of Zagreb, while studying Mathematics. As almost every other student, I got a little bit frustrated with Gram-Schmidt orthonormalization algorithm which after only few iterations gets VERY ugly numbers. To help myself and fellow colleagues, I have written a C++ implementation of Gram Schmidt algorithm on various Vector Spaces (such as multidimensional R and quadratic matrices, later I plan to add polynomials and multidimensional C).

### How do I get set up? ###

It was originally built with Visual Studio 2015, so that will work. After compilation, add your input in Input directory named "input.txt".
Application has no real UI, so input is done with file input (this could be improved in future).

### TODO ###
Soon:
  Matrices input

In future:
  Multidimensional C
  Polynomials with various scalar products specified
  Better UI

  
  
